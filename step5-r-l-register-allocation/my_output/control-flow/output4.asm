; Symbol table GLOBAL
; name a type INT location 0x20000000
; name b type INT location 0x20000004
; name c type INT location 0x20000008
; name d type INT location 0x2000000c
; Function: INT main([])

; Symbol table main

LI $t1, 1
MV $ga, $t1
LI $t2, 2
MV $gb, $t2
LI $t3, 3
MV $gc, $t3
LI $t4, 2
ADD $t5, $gb, $gc
MUL $t6, $t4, $t5
DIV $t7, $t6, $ga
MV $gd, $t7
LI $t8, 10
BNE $gd, $t8, out_1
PUTI $ga
out_1:
PUTI $gd
LI $t9, 0
MV $l8, $t9
J func_ret_main

.section .text
;Current temp: null
;IR Code: 
MV fp, sp
JR func_main
HALT
;
func_main:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, -28
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
SW x7, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Ensurehack
;Found free register x4
LI x4, 1
;Ensurehack
;Found free register x5
MV x5, x4
;Ensurehack
;Found free register x6
LI x6, 2
;Ensurehack
;Found free register x7
MV x7, x6
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
SW x4, -4(fp)
LI x4, 3
;Ensurehack
;Register is full, will allocate x5
;Freeing x5
LA x3, 0x20000000
SW x5, 0(x3)
MV x5, x4
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
SW x4, -8(fp)
LI x4, 2
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
SW x4, -12(fp)
ADD x4, x7, x5
;Register is full, will allocate x4
;Freeing x4
SW x4, -16(fp)
LW x4, -12(fp)
;Register is full, will allocate x5
;Freeing x5
LA x3, 0x20000008
SW x5, 0(x3)
LW x5, -16(fp)
;Ensurehack
;Register is full, will allocate x6
;Freeing x6
SW x6, -20(fp)
MUL x6, x4, x5
;Register is full, will allocate x4
;Freeing x4
LA x3, 0x20000000
LW x4, 0(x3)
;Ensurehack
;Register is full, will allocate x5
;Freeing x5
DIV x5, x6, x4
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
MV x4, x5
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
LA x3, 0x2000000c
SW x4, 0(x3)
LI x4, 10
;Register is full, will allocate x4
;Freeing x4
SW x4, -24(fp)
LA x3, 0x2000000c
LW x4, 0(x3)
;Register is full, will allocate x5
;Freeing x5
SW x5, -28(fp)
LW x5, -24(fp)
;Storing Register at the end of BB block;
LA x3, 0x20000004
SW x7, 0(x3)
BNE x4, x5, out_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x20000000
LW x4, 0(x3)
PUTI x4
;Storing Register at the end of BB block;
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_1:
;Found free register x4
LA x3, 0x2000000c
LW x4, 0(x3)
PUTI x4
;Ensurehack
;Found free register x5
LI x5, 0
;Ensurehack
;Found free register x6
MV x6, x5
;Storing Register at the end of BB block;
SW x6, 8(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_main
func_ret_main:
ADDI sp, sp, 4
LW x7, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;


.section .strings
