package assembly.instructions;

public class Register {
    
    private boolean free;   // If register is free
    private boolean dirty;  // If variable stored here is dirty
    private String var;     // Name of variable stored in this register
    private String name;    // Name of register
    
    public Register(boolean _free, boolean _dirty, String _name)
    {
        free = _free;
        dirty =_dirty;
        name = _name;
        var = "EMPTY";      
    }
    
    public void setFree(boolean _free)
    {
        free = _free;
    }
    
    
    public boolean isFree()
    {
        return free;
    }

    public void setDirty(boolean _dirty)
    {
        dirty = _dirty;   
    }

    public boolean isDirty()
    {
        return dirty;
    }

    public void setVarName(String _var)
    {
        var = _var;
    }
    public String getVarName()
    {
        return var;
    }

    public void setName(String _name)
    {
        name = _name;
    }

    public String getName()
    {
        return name;
    }
    

    @Override
    public String toString()
    {
        String ret = "";
        ret += "Register " + name + "\n";
        ret += "Store: " + var + "\n";
        ret += "free:" + free + "\n";
        ret += "dirty:" + dirty + "\n";
        ret += "\n";

        return ret;
    }
}
