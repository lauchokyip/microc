Found SHADOW adding INT a
Found SHADOW adding INT b
; Symbol table GLOBAL
; name true type STRING location 0x10000000 value "True\n"
; name false type STRING location 0x10000004 value "False\n"
; name a type FLOAT location 0x20000000
; name b type FLOAT location 0x20000004
; name ptr3 type PTR to INT location 0x20000008
; Function: INT main([])

; Symbol table main
; name a type INT location -4
; name b type INT location -8
; name ptr type PTR to INT location -12
; name ptr2 type PTR to PTR to INT location -16

Function main
	Statement list:
		AssignNode:
			VarNode: a
			IntLitNode: 7
		AssignNode:
			VarNode: ptr
			AddrOf: 
				VarNode: a
		AssignNode:
			PtrDeref: 
				BinaryOpNode: ADD
					VarNode: ptr
					IntLitNode: 4
			IntLitNode: 8
		Write: 
			VarNode: a
		AssignNode:
			VarNode: b
			BinaryOpNode: ADD
				PtrDeref: 
					VarNode: ptr
				IntLitNode: 2
		Write: 
			VarNode: b
		AssignNode:
			VarNode: ptr2
			AddrOf: 
				VarNode: ptr
		AssignNode:
			PtrDeref: 
				PtrDeref: 
					VarNode: ptr2
			IntLitNode: 9
		Write: 
			VarNode: a
		AssignNode:
			PtrDeref: 
				VarNode: ptr2
			AddrOf: 
				VarNode: b
		Write: 
			PtrDeref: 
				VarNode: ptr
		Return: 
			IntLitNode: 0
.section .text
;Current temp: null
;IR Code: 
MV fp, sp
JR func_main
HALT

func_main:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, -16
SW t1, 0(sp)
ADDI sp, sp, -4
SW t2, 0(sp)
ADDI sp, sp, -4
SW t3, 0(sp)
ADDI sp, sp, -4
SW t4, 0(sp)
ADDI sp, sp, -4
SW t5, 0(sp)
ADDI sp, sp, -4
SW t6, 0(sp)
ADDI sp, sp, -4
SW t7, 0(sp)
ADDI sp, sp, -4
SW t8, 0(sp)
ADDI sp, sp, -4
SW t9, 0(sp)
ADDI sp, sp, -4
SW t10, 0(sp)
ADDI sp, sp, -4
SW t11, 0(sp)
ADDI sp, sp, -4
SW t12, 0(sp)
ADDI sp, sp, -4
SW t13, 0(sp)
ADDI sp, sp, -4
SW t14, 0(sp)
ADDI sp, sp, -4
SW t15, 0(sp)
ADDI sp, sp, -4
SW t16, 0(sp)
ADDI sp, sp, -4
SW t17, 0(sp)
ADDI sp, sp, -4
SW t18, 0(sp)
ADDI sp, sp, -4
SW t19, 0(sp)
ADDI sp, sp, -4
SW t20, 0(sp)
ADDI sp, sp, -4
SW t21, 0(sp)
ADDI sp, sp, -4
SW t22, 0(sp)
ADDI sp, sp, -4
LI t1, 7
SW t1, -4(fp)
ADDI t2, fp, -4
SW t2, -12(fp)
LW t4, -12(fp)
LI t3, 4
ADD t5, t4, t3
LI t6, 8
SW t6, 0(t5)
LW t7, -4(fp)
PUTI t7
LW t8, -12(fp)
LW t8, -12(fp)
LW t10, 0(t8)
LI t9, 2
ADD t11, t10, t9
SW t11, -8(fp)
LW t12, -8(fp)
PUTI t12
ADDI t13, fp, -12
SW t13, -16(fp)
LW t14, -16(fp)
LW t15, 0(t14)
LI t16, 9
SW t16, 0(t15)
LW t17, -4(fp)
PUTI t17
LW t18, -16(fp)
ADDI t19, fp, -8
SW t19, 0(t18)
LW t20, -12(fp)
LW t21, 0(t20)
PUTI t21
LI t22, 0
SW t22, 8(fp)
J func_ret_main
func_ret_main:
ADDI sp, sp, 4
LW t22, 0(sp)
ADDI sp, sp, 4
LW t21, 0(sp)
ADDI sp, sp, 4
LW t20, 0(sp)
ADDI sp, sp, 4
LW t19, 0(sp)
ADDI sp, sp, 4
LW t18, 0(sp)
ADDI sp, sp, 4
LW t17, 0(sp)
ADDI sp, sp, 4
LW t16, 0(sp)
ADDI sp, sp, 4
LW t15, 0(sp)
ADDI sp, sp, 4
LW t14, 0(sp)
ADDI sp, sp, 4
LW t13, 0(sp)
ADDI sp, sp, 4
LW t12, 0(sp)
ADDI sp, sp, 4
LW t11, 0(sp)
ADDI sp, sp, 4
LW t10, 0(sp)
ADDI sp, sp, 4
LW t9, 0(sp)
ADDI sp, sp, 4
LW t8, 0(sp)
ADDI sp, sp, 4
LW t7, 0(sp)
ADDI sp, sp, 4
LW t6, 0(sp)
ADDI sp, sp, 4
LW t5, 0(sp)
ADDI sp, sp, 4
LW t4, 0(sp)
ADDI sp, sp, 4
LW t3, 0(sp)
ADDI sp, sp, 4
LW t2, 0(sp)
ADDI sp, sp, 4
LW t1, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET



.section .strings
0x10000000 "True\n"
0x10000004 "False\n"
