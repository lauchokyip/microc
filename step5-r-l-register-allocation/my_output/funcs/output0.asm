Found SHADOW adding FLOAT a
; Symbol table GLOBAL
; name true type STRING location 0x10000000 value "True\n"
; name false type STRING location 0x10000004 value "False\n"
; name a type FLOAT location 0x20000000
; name b type FLOAT location 0x20000004
; Function: INT main([])
; Function: FLOAT foo([INT, FLOAT])

; Symbol table main
; name c type INT location -4
; name d type INT location -8

; Symbol table foo
; name y type FLOAT location 12
; name x type INT location 16
; name d type INT location -4
; name f type FLOAT location -8
; name a type FLOAT location -12

FIMM.S $f1, 3.0
FMV.S $ga, $f1
FIMM.S $f2, 2.0
FMV.S $gb, $f2
LI $t1, 7
MV $l-4, $t1
LI $t2, 2
MUL $t3, $l-4, $t2
MV $l-8, $t3
FLT.S $t4, $ga, $gb
BNE $t4, x0, else_1
PUTI $l-8
PUTS $gtrue
J out_1
else_1:
PUTI $l-8
PUTS $gfalse
out_1:
LI $t5, 0
MV $l8, $t5
J func_ret_main

FADD.S $f1, $l12, $l16
FMV.S $l8, $f1
J func_ret_foo

.section .text
;Current temp: null
;IR Code: 
MV fp, sp
JR func_main
HALT
;
func_main:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, -12
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
SW x7, 0(sp)
ADDI sp, sp, -4
FSW f0, 0(sp)
ADDI sp, sp, -4
FSW f1, 0(sp)
ADDI sp, sp, -4
FSW f2, 0(sp)
ADDI sp, sp, -4
FSW f3, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Ensurehack
FIMM.S f0, 3.0
;Ensurehack
FMV.S f1, f0
;Ensurehack
FIMM.S f2, 2.0
;Ensurehack
FMV.S f3, f2
;Ensurehack
;Found free register x4
LI x4, 7
;Ensurehack
;Found free register x5
MV x5, x4
;Ensurehack
;Found free register x6
LI x6, 2
;Ensurehack
;Found free register x7
MUL x7, x5, x6
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
SW x4, -12(fp)
MV x4, x7
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
SW x4, -8(fp)
FLT.S x4, f1, f3
;Storing Register at the end of BB block;
SW x5, -4(fp)
LA x3, 0x20000000
FSW f1, 0(x3)
LA x3, 0x20000004
FSW f3, 0(x3)
BNE x4, x0, else_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LW x4, -8(fp)
PUTI x4
;Found free register x5
LA x3, 0x10000000
PUTS x3
;Storing Register at the end of BB block;
J out_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
PUTI x4
PUTS x3
;Storing Register at the end of BB block;
J out_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
else_1:
;Found free register x4
LW x4, -8(fp)
PUTI x4
;Found free register x5
LA x3, 0x10000004
PUTS x3
;Storing Register at the end of BB block;
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_1:
;Ensurehack
;Found free register x4
LI x4, 0
;Ensurehack
;Found free register x5
MV x5, x4
;Storing Register at the end of BB block;
SW x5, 8(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_main
func_ret_main:
ADDI sp, sp, 4
FLW f3, 0(sp)
ADDI sp, sp, 4
FLW f2, 0(sp)
ADDI sp, sp, 4
FLW f1, 0(sp)
ADDI sp, sp, 4
FLW f0, 0(sp)
ADDI sp, sp, 4
LW x7, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;
func_foo:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, -12
FSW f0, 0(sp)
ADDI sp, sp, -4
FSW f1, 0(sp)
ADDI sp, sp, -4
FSW f2, 0(sp)
ADDI sp, sp, -4
FSW f3, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
FLW f0, 12(fp)
FLW f1, 16(fp)
;Ensurehack
FADD.S f2, f0, f1
;Ensurehack
FMV.S f3, f2
;Storing Register at the end of BB block;
FSW f3, 8(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_foo
func_ret_foo:
ADDI sp, sp, 4
FLW f3, 0(sp)
ADDI sp, sp, 4
FLW f2, 0(sp)
ADDI sp, sp, 4
FLW f1, 0(sp)
ADDI sp, sp, 4
FLW f0, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;


.section .strings
0x10000000 "True\n"
0x10000004 "False\n"
