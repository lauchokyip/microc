; Symbol table GLOBAL
; name welcome type STRING location 0x10000000 value "Compute GCD(x, y) using Euclid's algorithm \n"
; name xprompt type STRING location 0x10000004 value "Enter x: "
; name yprompt type STRING location 0x10000008 value "Enter y: "
; name result type STRING location 0x1000000c value "GCD(x, y) = "
; name x type INT location 0x20000000
; name y type INT location 0x20000004
; name mod type INT location 0x20000008
; name tmp type INT location 0x2000000c
; name prompt type STRING location 0x10000010 value "Enter a number: "
; Function: INT main([])

; Symbol table main

PUTS $gwelcome
PUTS $gxprompt
GETI $gx
PUTS $gyprompt
GETI $gy
loop_2:
LI $t1, 0
BEQ $gx, $t1, out_3
BGT $gx, $gy, out_1
MV $gtmp, $gx
MV $gx, $gy
MV $gy, $gtmp
out_1:
loop_1:
BLT $gx, $gy, out_2
SUB $t2, $gx, $gy
MV $gx, $t2
J loop_1
out_2:
PUTI $gx
PUTI $gy
J loop_2
out_3:
PUTS $gresult
PUTI $gy
LI $t3, 0
MV $l8, $t3
J func_ret_main

.section .text
;Current temp: null
;IR Code: 
MV fp, sp
JR func_main
HALT
;
func_main:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, 0
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
SW x7, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x10000000
PUTS x3
;Found free register x5
LA x3, 0x10000004
PUTS x3
;Ensurehack
;Found free register x6
GETI x6
;Found free register x7
LA x3, 0x10000008
PUTS x3
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
GETI x4
;Storing Register at the end of BB block;
LA x3, 0x20000004
SW x4, 0(x3)
LA x3, 0x20000000
SW x6, 0(x3)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
loop_2:
;Ensurehack
;Found free register x4
LI x4, 0
;Found free register x5
LA x3, 0x20000000
LW x5, 0(x3)
;Storing Register at the end of BB block;
BEQ x5, x4, out_3
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x20000000
LW x4, 0(x3)
;Found free register x5
LA x3, 0x20000004
LW x5, 0(x3)
;Storing Register at the end of BB block;
BGT x4, x5, out_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x20000000
LW x4, 0(x3)
;Ensurehack
;Found free register x5
MV x5, x4
;Found free register x6
LA x3, 0x20000004
LW x6, 0(x3)
;Ensurehack
MV x4, x6
;Ensurehack
MV x6, x5
;Storing Register at the end of BB block;
LA x3, 0x20000000
SW x4, 0(x3)
LA x3, 0x2000000c
SW x5, 0(x3)
LA x3, 0x20000004
SW x6, 0(x3)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_1:
;Storing Register at the end of BB block;
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
loop_1:
;Found free register x4
LA x3, 0x20000000
LW x4, 0(x3)
;Found free register x5
LA x3, 0x20000004
LW x5, 0(x3)
;Storing Register at the end of BB block;
BLT x4, x5, out_2
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x20000000
LW x4, 0(x3)
;Found free register x5
LA x3, 0x20000004
LW x5, 0(x3)
;Ensurehack
;Found free register x6
SUB x6, x4, x5
;Ensurehack
MV x4, x6
;Storing Register at the end of BB block;
LA x3, 0x20000000
SW x4, 0(x3)
J loop_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_2:
;Found free register x4
LA x3, 0x20000000
LW x4, 0(x3)
PUTI x4
;Found free register x5
LA x3, 0x20000004
LW x5, 0(x3)
PUTI x5
;Storing Register at the end of BB block;
J loop_2
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_3:
;Found free register x4
LA x3, 0x1000000c
PUTS x3
;Found free register x5
LA x3, 0x20000004
LW x5, 0(x3)
PUTI x5
;Ensurehack
;Found free register x6
LI x6, 0
;Ensurehack
;Found free register x7
MV x7, x6
;Storing Register at the end of BB block;
SW x7, 8(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_main
func_ret_main:
ADDI sp, sp, 4
LW x7, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;


.section .strings
0x10000000 "Compute GCD(x, y) using Euclid's algorithm \n"
0x10000004 "Enter x: "
0x10000008 "Enter y: "
0x1000000c "GCD(x, y) = "
0x10000010 "Enter a number: "
