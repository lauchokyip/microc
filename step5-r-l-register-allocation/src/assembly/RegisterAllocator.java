package assembly;

import compiler.LocalScope;
import compiler.Scope;
import java.util.*;

import assembly.instructions.*;
/**
* The Registor Allocator class will implements register allocation 
* that takes in number of resgister to be used 
* 
*
* @author  Chok Yip Lau , Ruochong Wu
* @version 1.0
* @since   2020-10-27 
*/
public class RegisterAllocator {

    /** ADD REGISTER ALLOCATION HERE
     * 
     * You may find it useful to do this in the following way:
     * 
     * 1. Write a register allocator class that is initialized with the number of int/fp registers to use, the code from
     * 		`body`, and the function scope from `node` (the function scope gives you access to local/global variables)
     * 2. Within the register allocator class, do the following
     * 		a. Split the code in body into basic blocks
     * 		b. (573 version) Perform liveness analysis on each basic block (assume globals and locals are live)
     * 		b. (468 version) Assume all locals/globals/temporaries are live all the time
     * 		c. Perform register allocation on each basic block using the algorithms presented in class,
     * 			converting 3AC into assembly code with macro expansion
     * 			i. Add code to track the state of the registers for each basic block (what is assigned to the register, whether it's dirty)
     * 			ii. As you perform register allocation within a basic block, spill registers to memory as necessary. Use any
     * 				heuristic you want to determine which registers to allocate and which to spill
     * 			iii. If you need to spill a temporary to memory, you'll find it easiest to add the temporary as a new "local" variable
     * 				to the local scope (you can just use the temporary name as the variable name); that will automatically allocate a spot
     * 				in the activation record for it.
     * 			iv. At the end of each basic block, save all dirty/live registers that hold globals/locals back to the stack
     * 3. Once register allocation is done, track:
     * 		a. How big the local scope is after spilling temporaries -- this affects allocating the stack frame
     * 		b. How many total registers you used -- this affects the register save/restore code
     * 4. Now generate code for your function as before, but using the updated information for register save/restore and frame allocation
     */

    private final int DEBUG = 0;
    private int numOfIRegister;
    private int numOfFpRegister;
    private CodeObject body;
    private CodeObject translated;
    
    private LocalScope functionScope;

    private ArrayList <Integer> leaders = new ArrayList <Integer>();
    private ArrayList <InstructionList> blocks = new ArrayList <InstructionList>();
    private String [] availIRegisters = {"x4","x5","x6","x7","x9","x10","x11","x12","x13","x14","x15","x16","x17","x18","x19","x20",
                                 "x21","x22","x23","x24","x25","x26","x27","x28","x29","x30","x31"};
    private String [] availFpRegisters = {"f0","f1","f2","f3","f4","f5","f6","f7","f8","f9","f10","f11","f12","f13","f14","f15","f16","f17","f18","f19",
                                  "f20","f21","f22","f23","f24","f25","f26","f27","f28","f29","f30","f31"};
    
    private Register [] intRegisters;
    private Register [] fpRegisters;

    private HashMap<String, Integer> intRegisterHashMap = new HashMap<String, Integer>();
    private HashMap<String, Integer> floatRegisterHashMap = new HashMap<String, Integer>();

    private Stack<String> availIntegerRegisterStack;
    private Stack<String> availFloatRegisterStack;

    private Set<String> instructionRegisters; // Set containing registers used by current instruction
    private Set<String> usedIntRegisters = new HashSet<String>();
    private Set<String> usedFpRegisters = new HashSet<String>();

    private Instruction jumpInstruction;
    
    public RegisterAllocator(int _numOfIRegister, int _numOfFpRegister , CodeObject _body, LocalScope _functionScope)
    {

        numOfIRegister = _numOfIRegister - 5;
        numOfFpRegister = _numOfFpRegister;
        body = _body;
        functionScope = _functionScope;

        intRegisters = new Register [numOfIRegister];
        fpRegisters = new Register [numOfFpRegister];

        availIntegerRegisterStack = new Stack<String>();
        availFloatRegisterStack = new Stack<String>();


        initIntegerRegisters();
        initFloatRegisters();



        translated = new CodeObject();
        
    }

    /**
     * Method that will return the code object
     * 
     * @param Nothing
     * @return translated codeObject
     */
    public CodeObject getTranslatedCodeObject() 
    {
        findLeaders();
        buildBasicBlocks();
        processBlocks();
        translated.code.add(jumpInstruction);
        return translated;
    }

    /**
     * Method that will check if the instruction is f
     * We will accomplish by checking if there is an f
     * 
     * @param opCode, dest, src1, src2
     * @return boolen
     */  
    public boolean isOpCodeFloat(Instruction.OpCode opCode, String dest, String src1, String src2)
    {
        String opCodeStr = opCode.name();
        // check the first char and the last char
        if(opCodeStr.charAt(0) == 'F' || opCodeStr.charAt(opCodeStr.length() - 1) == 'F')
            return true;

        // f one of the operand is f, then the instruction is surely float
        else if(dest != null && dest.charAt(1) == 'f')
            return true;
        else if(src1 != null && src1.charAt(1) == 'f')
            return true;
        else if(src2 != null && src2.charAt(1) == 'f')
            return true;

        return false;
    }

    /**
     * Method that will perform register allocation on one basic block 
     * 
     * @param Basic Block instruction list
     * @return Nothing
     */
    private void processBB(InstructionList block)
    {
        String src1;
        String src2;
        String dest;
        String temp;
        Register src1Reg;
        Register src2Reg;
        Register destReg;
        boolean isFloat;
        Instruction branchInstruction = null;
        Instruction eachInstruction;

        translated.code.add(new Blank("//////////////////////////////////////////"));
        translated.code.add(new Blank("Starting BB;"));

        for(int i = 0 ; i < block.size(); i++)
        {
            eachInstruction = (Instruction) block.toArray()[i];
            instructionRegisters = new HashSet<String>();
            
            if(eachInstruction.is3AC())
            {  
                /**
                 * Step1 : Check if each operand is a 3AC
                 * Step2 : if operand is not set then ignore, if it's set continue step3
                 * Step3 : perform ensure or allocate
                 * Step4 : reset the instruction
                 * Step5 : add the resgitser name into the set so we won't reuse it again
                 */
                src1 = eachInstruction.getOperand(Instruction.Operand.SRC1);
                src2 = eachInstruction.getOperand(Instruction.Operand.SRC2);
                dest = eachInstruction.getOperand(Instruction.Operand.DEST);
                isFloat = isOpCodeFloat(eachInstruction.getOC(),dest, src1, src2);
                if(DEBUG > 0)
                {
                    System.out.print(eachInstruction);
                    System.out.println(" " + "isFloat is" + isFloat);
                }

                // Exceptions first
                // I am hackerman

                if (eachInstruction instanceof Fsw || eachInstruction instanceof Sw) // Swap src1 and dest around
                {
                    temp = src1;
                    src1 = dest;
                    dest = temp;
                }
                else
                {
                    
                }

                if(src1 != null && src1.contains("$"))
                {
                    
                    if(eachInstruction.getOC().name() == "PUTS")
                    {
                        src1Reg = ensure(src1, isFloat);
                        instructionRegisters.add(src1Reg.getName());
                        eachInstruction.setSrc1("x3");
                    }
                    else
                    {
                        src1Reg = ensure(src1, isFloat);
                        if (eachInstruction instanceof Fsw || eachInstruction instanceof Sw)    //Exception for Fsw and Sw: src1 is actually dest
                        {
                            eachInstruction.setDest(src1Reg.getName());
                        }
                        else
                        {
                            eachInstruction.setSrc1(src1Reg.getName());
                            instructionRegisters.add(src1Reg.getName());
                        }
                        
                    }
                }
                
                if (src2 != null && src2.contains("$"))
                {
                    src2Reg = ensure(src2, isFloat);
                    eachInstruction.setSrc2(src2Reg.getName());
                    instructionRegisters.add(src2Reg.getName());   
                }
                    
                if (dest != null && dest.contains("$"))
                {

                    if (dest.charAt(1) == 't')
                    {
                        destReg = ensureHack(dest, false);
                    }
                    else if(dest.charAt(1) == 'f')
                    {
                        destReg = ensureHack(dest, true);
                    }
                    else
                    {
                        destReg = ensureHack(dest, isFloat);
                    }


                    // destReg = ensureHack(dest, isFloat);

                    // generate opCode

                    if (eachInstruction instanceof Fsw || eachInstruction instanceof Sw)    //Exception for Fsw and Sw: dest is actually src1
                    {
                        eachInstruction.setSrc1(destReg.getName());
                        instructionRegisters.add(destReg.getName());
                    }
                    else
                    {
                        eachInstruction.setDest(destReg.getName());
                    }
                    destReg.setDirty(true);
                }


                // more hack...... if it's jump or brancg instruction and end of basic block wait to add it
                if(i == block.size() - 1)
                {
                    if(eachInstruction instanceof Beq || eachInstruction instanceof Bge || eachInstruction instanceof Bgt || eachInstruction instanceof Ble || 
                       eachInstruction instanceof Blt || eachInstruction instanceof Bne)
                    {
                        branchInstruction = eachInstruction;
                        break;
                    }
                    else
                    {
                        translated.code.add(eachInstruction);
                        break;
                    }
                }
                
                translated.code.add(eachInstruction);
            }
            else    // Not 3AC, no translation necessary
            {
                //another crazy hack
                if(eachInstruction instanceof J)
                {
                    branchInstruction = eachInstruction;
                }
                else
                {
                    translated.code.add(eachInstruction);
                }
                
            }
            

        }
        translated.code.add(new Blank("Storing Register at the end of BB block;"));

        // Loop through registers, save all dirty registers
        for (Register eachReg : intRegisters)
        {
            if (eachReg.getVarName().charAt(1) == 't')
            {
                continue;
            }
            else if (eachReg.isDirty())
            {
                translated.code.addAll(store(eachReg,false));
                //mark as not dirty after store
                eachReg.setDirty(false);
            }

        } 

        for (Register eachReg : fpRegisters)
        {
            if (eachReg.getVarName().charAt(1) == 'f')
            {
                continue;
            }
            else if (eachReg.isDirty())
            {
                translated.code.addAll(store(eachReg, true));
                //mark as not dirty after store
                eachReg.setDirty(false);
            }
        }

        if(branchInstruction != null)
        {
            translated.code.add(branchInstruction);
        }
        
        availIntegerRegisterStack = new Stack<String>();
        availFloatRegisterStack = new Stack<String>();

        // now set every register to free and clean
        for(int i = numOfIRegister -1; i >= 0; i--)
        {
            intRegisters[i].setDirty(false);
            free(intRegisters[i].getName());
        }
        for(int i = numOfFpRegister -1; i >= 0; i--)
        {
            fpRegisters[i].setDirty(false);
            free(fpRegisters[i].getName());
        }
        
        
        translated.code.add(new Blank("End BB;"));
        translated.code.add(new Blank("//////////////////////////////////////////"));
    }
    
    /**
     * Method that will process all blocks
     * 
     * @param Nothing
     * @return Nothing
     */
    private void processBlocks()
    {
        for(InstructionList eachBB : blocks)
        {
            processBB(eachBB);
        }
    }


    /**
     * Method that will build basic block 
     * 
     * @param Nothing
     * @return Nothing
     */
    private void buildBasicBlocks()
    {
        ArrayList <Instruction> instructionList = new ArrayList<Instruction>(body.getCode());
        // Dirty hack
        instructionList.remove(instructionList.size()-1);
        //

        for (int i = 0; i < leaders.size(); i++)
        {
            InstructionList temp = new InstructionList();
            temp.add(instructionList.get(leaders.get(i)));

            // the loop the get the rest of the instruction and add it to the block
            for (int j = leaders.get(i) + 1; (j < instructionList.size()) && (!leaders.contains(j)); j++)
            {
                temp.add(instructionList.get(j));
            }
            blocks.add(temp);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // DEBUG
        //
        /////////////////////////////////////////////////////////////////////////////////
        if(DEBUG > 0)
        {
            System.out.println("; Printing out basic blocks for " + functionScope.getName());
            for(int i = 0; i < blocks.size(); i++)
            {
                System.out.println("; ====================");
                for(int j = 0 ; j < blocks.get(i).size(); j++)
                {
                    System.out.println(";" + blocks.get(i).toArray()[j]);
                }
            }            
        }
    }

    /**
     * Method that will build find the leaders and modify leader Arraylist 
     * 
     * @param Nothing
     * @return Nothing
     */
    public void findLeaders()
    {
        ArrayList <Instruction> instructionList = new ArrayList<Instruction>(body.getCode());
        // Dirty hack
        jumpInstruction = instructionList.get(instructionList.size() - 1);
        instructionList.remove(instructionList.size()-1);
        //
        leaders.add(0);
        for(int i = 1; i < instructionList.size(); i++) {
            
            Instruction eachInstruction = instructionList.get(i);
            Instruction nextInstruction;

            if(eachInstruction instanceof Beq || eachInstruction instanceof Bge || eachInstruction instanceof Bgt || eachInstruction instanceof Ble || 
               eachInstruction instanceof Blt || eachInstruction instanceof Bne || eachInstruction instanceof Feq || eachInstruction instanceof Fle || 
               eachInstruction instanceof Flt || eachInstruction instanceof J)
            {
                // will this cause any errors?
                if ( i < instructionList.size()-1 && !leaders.contains(instructionList.get(i + 1)))   // Check first to see if i + 1 is in range
                {
                    nextInstruction = instructionList.get(i+1);
                    // this is for float
                    if(nextInstruction instanceof Beq || nextInstruction instanceof Bne)
                    {
                        leaders.add(i + 2);
                    }
                    else
                    {
                        leaders.add(i + 1);
                    }
                    
                }       
            }
            else if (eachInstruction instanceof Label)
            {
                if (!leaders.contains(i))
                {
                    leaders.add(i);
                }       
            }
     
        }
        ////////////////////////////////////////////////////////////////////////////////
        // DEBUG
        //
        /////////////////////////////////////////////////////////////////////////////////
        if(DEBUG > 0)
        {
            // System.out.println(leaders);
            System.out.println("; Printing Leaders for " + functionScope.getName() );
            for(int i = 0 ; i < leaders.size(); i++)
            {
                int instructionIndex = leaders.get(i);
                System.out.println(";" + instructionList.get(instructionIndex));
            }
    
        }
    }


    /**
     * Method that will initialize int registers
     * 
     * @param None
     * @return None
     */
    private void initIntegerRegisters()
    {
        for (int i = 0 ; i < numOfIRegister; i++)
        {
            // allocate new integer Register
            intRegisters[i] = new Register(true, false, availIRegisters[i]);
            // set up hasMap for O(1) search
            intRegisterHashMap.put(availIRegisters[i], i); 
                
            if(DEBUG > 0)
            {
                System.out.println("/********************/");
                System.out.println(intRegisters[i]);
                System.out.println(intRegisterHashMap);
                System.out.println("/********************/");
            }
            
        }

         // set up stack
        for(int i = numOfIRegister - 1; i >= 0; i--)
        {
            availIntegerRegisterStack.push(availIRegisters[i]);
        }
    }
    
    /**
     * Method that will initialize float registers
     * 
     * @param None
     * @return None
     */
    private void initFloatRegisters()
    {
        for (int i = 0 ; i < numOfFpRegister; i++)
        {
            fpRegisters[i] = new Register(true, false, availFpRegisters[i]); 
            floatRegisterHashMap.put(availFpRegisters[i], i);
            
            if(DEBUG > 0)
            {
                System.out.println("/********************/");
                System.out.println(fpRegisters[i]);
                System.out.println(floatRegisterHashMap);
                System.out.println("/********************/");
            }
        }

        // set up stack
        for(int i = numOfFpRegister - 1; i >= 0; i--)
        {
            availFloatRegisterStack.push(availFpRegisters[i]);
        }
    }

    /**
     * Method that will free given register and generate code to store if necessary
     * 
     * @param Name of register
     * @return None
     */
    private void free(String registerName)
    {
 
        // Lookup index of given registerName
        int index;
        Register temp;
        InstructionList il = new InstructionList();

        translated.code.add(new Blank("Freeing " + registerName));
     
        if(registerName.charAt(0) == 'x')
        {
            //push it back to free register stack
            availIntegerRegisterStack.push(registerName);

            //it's a integer register
            index = intRegisterHashMap.get(registerName);
            
            //Check if register is dirty
            temp = intRegisters[index];
            if (temp.isDirty())
            {
                // generate store if dirty                         
                il.addAll(store(temp, false));
                
                // Insert store instruction into translated CodeObject
                translated.code.addAll(il);
            }
            
        }
        else
        {
            //push it back to free register stack
            availFloatRegisterStack.push(registerName);

            //it's a float register
            index = floatRegisterHashMap.get(registerName);
            
            //Check if register is dirty
            temp = fpRegisters[index];
            if (temp.isDirty())
            {
                // generate store if dirty                           
                il.addAll(store(temp,true));
                // Insert store instruction into translated CodeObject
                translated.code.addAll(il);
            }
        }

        temp.setDirty(false);
        temp.setVarName("EMPTY");
        temp.setFree(true);
    }

    /**
     * Method that will allocate a register for an operand
     * (Have to know what the first operand register is)
     * 
     * @param String varName
     * @return the allocated register
     */
    private Register allocate(String varName, boolean isFloat)
    {
        Register allocatedRegister = null; 
        int registerIndex;
        String registerName;

        if(DEBUG > 0)
        {
            System.out.println("allocate: " + varName);
        }
 
        if(!isFloat)    
        {
            //pop the first available register
            if(!availIntegerRegisterStack.empty())
            {
                registerName = availIntegerRegisterStack.pop();
                registerIndex = intRegisterHashMap.get(registerName);
                allocatedRegister = intRegisters[registerIndex];

                translated.code.add(new Blank("Found free register " + registerName));

                allocatedRegister.setFree(false);
                allocatedRegister.setDirty(false);
                allocatedRegister.setVarName(varName);

                usedIntRegisters.add(allocatedRegister.getName());
                return allocatedRegister;

            }

            for (int i = 0; i < numOfIRegister; i++)
            {
                if (instructionRegisters.contains(availIRegisters[i]))
                {
                    // if it's ensure for A and B don't do anything
                   
                    continue;
                }   

                translated.code.add(new Blank("Register is full, will allocate " + availIRegisters[i]));
                
                registerName = availIRegisters[i];
                registerIndex = i;
                free(registerName);
                 // Bug: bc we have allocate that's why we have to pop again
                availIntegerRegisterStack.pop();
                allocatedRegister = intRegisters[i];
                

                allocatedRegister.setFree(false);
                allocatedRegister.setDirty(false);
                allocatedRegister.setVarName(varName);

                usedIntRegisters.add(allocatedRegister.getName());
                break;
            }
        }
        else // have to use float operand
        {
            //pop the first available register
            if(!availFloatRegisterStack.empty())
            {
                registerName = availFloatRegisterStack.pop();
                registerIndex = floatRegisterHashMap.get(registerName);
                allocatedRegister = fpRegisters[registerIndex];

                allocatedRegister.setFree(false);
                allocatedRegister.setDirty(false);
                allocatedRegister.setVarName(varName);

                usedFpRegisters.add(allocatedRegister.getName());
                return allocatedRegister;
            }

            for (int i = 0; i < numOfFpRegister; i++)
            {
                if (instructionRegisters.contains(availFpRegisters[i]))
                {
                    // don't do anything
                    continue;
                }
                registerName = availFpRegisters[i];
                registerIndex = i;
                free(registerName);
                allocatedRegister = fpRegisters[i];
                // Bug: bc we have allocate that's why we have to pop again
                availFloatRegisterStack.pop();

                allocatedRegister.setFree(false);
                allocatedRegister.setDirty(false);
                allocatedRegister.setVarName(varName);

                usedFpRegisters.add(allocatedRegister.getName());
                break;
            }
        }
        

        return allocatedRegister;
    }

    /**
     * Method that will ensure an operand is in register
     * 
     * @param String varName
     * @return ensured Register
     */
    public Register ensure(String varName, boolean isFloat)
    {
        Register ensuredRegister; 

        if(DEBUG > 0)
        {
            System.out.println("ensure: " + varName);
        }
        if(!isFloat)
        {
            for (int i = 0; i < numOfIRegister; i++)
            {
                ensuredRegister = intRegisters[i];
                if (ensuredRegister.getVarName().equals(varName))
                {
                    return ensuredRegister;
                }
            }
            
            ensuredRegister = allocate(varName, isFloat);
            translated.code.addAll(load(ensuredRegister, varName, isFloat));
        }
       else // have to use float operand
        {
            for (int i = 0; i < numOfFpRegister ; i++)
            {
                ensuredRegister = fpRegisters[i];
                if (ensuredRegister.getVarName().equals(varName))
                {
                   return ensuredRegister;
                }
            }
            ensuredRegister = allocate(varName, isFloat);
            translated.code.addAll(load(ensuredRegister, varName, isFloat));    
        }

        return ensuredRegister;
    }

    /**
     * Method that will ensure an operand is in register but without loading
     * 
     * @param String varName
     * @return ensured Register
     */
    public Register ensureHack(String varName, boolean isFloat)
    {
        Register ensuredRegister; 
        if(DEBUG > 0)
        {
            System.out.println("ensureHack: " + varName);
        }

        translated.code.add(new Blank("Ensurehack"));

        if(!isFloat)
        {
            for (int i = 0; i < numOfIRegister; i++)
            {
                ensuredRegister = intRegisters[i];
                if (ensuredRegister.getVarName().equals(varName))
                {
                    return ensuredRegister;
                }
            }
            
            ensuredRegister = allocate(varName, isFloat);
        }
       else // have to use float operand
        {
            for (int i = 0; i < numOfFpRegister ; i++)
            {
                ensuredRegister = fpRegisters[i];
                if (ensuredRegister.getVarName().equals(varName))
                {
                   return ensuredRegister;
                }
            }
            ensuredRegister = allocate(varName, isFloat);
        }

        return ensuredRegister;
    }

    /**
     *  Method that will store the varName in the resgiter back to the memory 
     *  varName can be global, temp, local
     *  @param varName (String)
     *  @return InstructionList
     */ 
    private InstructionList store(Register registerToStore, boolean isFloat)
    {
        InstructionList il = new InstructionList();
        String address;
        String framePtrOffset;
        String varName = registerToStore.getVarName();
        String registerName = registerToStore.getName();

        // $g<varName> if it's a global 
        if(varName.charAt(1) == 'g')
        {
            varName = varName.substring(2);
            address = functionScope.getSymbolTableEntry(varName).addressToString();
            il.add(new La("x3",address));

            // check if it's int or float before we store it back
            if(!isFloat)
            {
                il.add(new Sw(registerName, "x3", "0"));
            }
            else
            {
                il.add(new Fsw(registerName, "x3", "0"));
            }

            return il;
        }
        // $l<frame ptr offset> local variable (just store it back to the offest of frame pointer)
        else if(varName.charAt(1) == 'l')
        {
            framePtrOffset = varName.substring(2);
            // check if it's int or float before we store it back
            if(!isFloat)
            {
                il.add(new Sw(registerName, "fp", framePtrOffset));
            }
            else
            {
                il.add(new Fsw(registerName, "fp", framePtrOffset));
            }

            return il;
        }
        // $t<number> temp variable (int) (have to add it our symbol table and store it in stack)
        else if(varName.charAt(1) == 't')
        {
            // add new entry to the SymbolTable
            functionScope.addSymbol(Scope.Type.INT, varName.substring(1));
            framePtrOffset = functionScope.getSymbolTableEntry(varName.substring(1)).addressToString();
            il.add(new Sw(registerName, "fp", framePtrOffset));
            
            return il;
        }
        // $f<number> temp variable (float) same as above
        functionScope.addSymbol(Scope.Type.FLOAT, varName.substring(1));
        framePtrOffset = functionScope.getSymbolTableEntry(varName.substring(1)).addressToString();
        il.add(new Fsw(registerName, "fp", framePtrOffset));

        return il;

    }



    /**
     *  Method that will load the varName into the resgiter 
     *  varName can be global, temp, local
     *  @param varName (String)
     *  @return InstructionList
     */ 
    private InstructionList load(Register registerToLoad, String varName, boolean isFloat)
    {

        InstructionList il = new InstructionList();
        String address;
        String framePtrOffset;
        String registerName = registerToLoad.getName();

        // $g<varName> if it's a global 
        if(varName.charAt(1) == 'g')
        {
            varName = varName.substring(2);
            address = functionScope.getSymbolTableEntry(varName).addressToString();
            il.add(new La("x3",address));

            // check if it's int or float before we store it back
            if(functionScope.getSymbolTableEntry(varName).getType() != Scope.Type.STRING)
            {
                if(!isFloat)
                {
                    il.add(new Lw(registerName, "x3", "0"));
                }
                else
                {
                    il.add(new Flw(registerName, "x3", "0"));
                }
            }
           

            return il;
        }
        // $l<frame ptr offset> local variable (just store it back to the offest of frame pointer)
        else if(varName.charAt(1) == 'l')
        {
            framePtrOffset = varName.substring(2);
            // check if it's int or float before we store it back
            if(!isFloat)
            {
                il.add(new Lw(registerName, "fp", framePtrOffset));
            }
            else
            {
                il.add(new Flw(registerName, "fp", framePtrOffset));
            }

            return il;
        }
        
        // $t<number> temp variable (int) (alr on stack from allocate)
        else if(varName.charAt(1) == 't')
        {
            varName = varName.substring(1);
            framePtrOffset = functionScope.getSymbolTableEntry(varName).addressToString();
            il.add(new Lw(registerName, "fp", framePtrOffset));
            
            return il;
        }
        // $f<number> temp variable (float) same as above
        varName = varName.substring(1);
        framePtrOffset = functionScope.getSymbolTableEntry(varName).addressToString();
        il.add(new Flw(registerName, "fp", framePtrOffset));

        return il;
    }



    /**
     * Method that will return number of integer registers used
     * 
     */
    public int getNumUsedIntRegisters()
    {
        return usedIntRegisters.size();
    }


    /**
     * Method that will return number of float registers used
     * 
     */
    public int getNumUsedFpRegisters()
    {
        return usedFpRegisters.size();
    }



    /**
     * Debugging purpose: print out the code in translated CodeObject
     * 
     */
    private void printOutTranslatedCode()
    {
        System.out.println(translated.code);
    }
    
    /**
     * Debugging purpose: print out each Integer resgister value
     * 
     */
    private void printEachIntRegisterValue()
    {
        System.out.println("");
        System.out.println("/*********************************/");
        for(int i = 0; i < numOfIRegister; i++ )
        {
          
            System.out.println("Register" + intRegisters[i].getName() + ": " + intRegisters[i].getVarName() + " isDirty: " +  intRegisters[i].isDirty());
        }
        System.out.println("/*********************************/");
    }

    /**
     * Debugging purpose: print out each Float resgister value
     * 
     */
    private void printEachFloatRegisterValue()
    {
        System.out.println("");
        System.out.println("/*********************************/");
        for(int i = 0; i < numOfFpRegister; i++ )
        {
            System.out.println("Register" + fpRegisters[i].getName() + ": " + fpRegisters[i].getVarName() + " isDirty: " +  fpRegisters[i].isDirty());
        }
        System.out.println("/*********************************/");
    }




}

