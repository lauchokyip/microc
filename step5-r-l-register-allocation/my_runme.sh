#!/bin/bash

mkdir -p my_output/funcs my_output/basic my_output/funcs

echo "Testing basic block.........."
for i in {0..10}
do
    java -cp classes:lib/antlr-4.8-complete.jar compiler.Compiler tests/basic/test$i.uC 32 > my_output/basic/output$i.asm
done

for i in {0..10}
do
    echo "****************"
    echo "our basic/test$i"
    python3 RiscSim/driver.py my_output/basic/output$i.asm
    printf "\n" 
    echo "expected basic/test$i"
    python3 RiscSim/driver.py outputs/basic/test$i.asm
    echo "****************" 
    printf "\n"
done

echo "Testing control-flow"
for i in {0..8}
do
    echo "running test$i"
    java -cp classes:lib/antlr-4.8-complete.jar compiler.Compiler tests/control-flow/test$i.uC 9 > my_output/control-flow/output$i.asm
done

for i in {0..8}
do
    echo "****************"
    echo "our control-flow/test$i"
    python3 RiscSim/driver.py my_output/control-flow/output$i.asm
    printf "\n" 
    echo "expected control-flow/test$i"
    python3 RiscSim/driver.py outputs/control-flow/test$i.asm
    echo "****************" 
    printf "\n"
done
printf "\n"

echo "Testing funcs"
for i in {0..5}
do
    echo "running test$i"
    java -cp classes:lib/antlr-4.8-complete.jar compiler.Compiler tests/funcs/test$i.uC 9 > my_output/funcs/output$i.asm
done

for i in {0..5}
do
    echo "****************"
    echo "our funcs/test$i"
    python3 RiscSim/driver.py my_output/funcs/output$i.asm
    printf "\n" 
    echo "expected funcs/test$i"
    python3 RiscSim/driver.py outputs/funcs/test$i.asm
    echo "****************" 
    printf "\n"
done
printf "\n"