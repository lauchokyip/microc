; Symbol table GLOBAL
; name curVal type FLOAT location 0x20000000
; name x type FLOAT location 0x20000004
; name degree type INT location 0x20000008
; Function: FLOAT poly([FLOAT, FLOAT, INT])
; name val type STRING location 0x10000000 value "Enter x value to evaluate: "
; name degreePrompt type STRING location 0x10000004 value "Enter a degree: "
; name prompt type STRING location 0x10000008 value "Enter coefficient: "
; Function: INT main([])

; Symbol table main
; name cur type INT location -4

; Symbol table poly
; name degree type INT location 12
; name x type FLOAT location 16
; name curVal type FLOAT location 20
; name coeff type FLOAT location -4

FIMM.S $f1, 0.0
FMV.S $gcurVal, $f1
LI $t1, 0
MV $l-4, $t1
PUTS $gval
GETF $gx
PUTS $gdegreePrompt
GETI $gdegree
FSW $gcurVal, 0(sp)
ADDI sp, sp, -4
FSW $gx, 0(sp)
ADDI sp, sp, -4
SW $gdegree, 0(sp)
ADDI sp, sp, -4
ADDI sp, sp, -4
SW ra, 0(sp)
ADDI sp, sp, -4
JR func_poly
ADDI sp, sp, 4
LW ra, 0(sp)
ADDI sp, sp, 4
FLW $f2, 0(sp)
ADDI sp, sp, 12
FMV.S $gcurVal, $f2
PUTF $gcurVal
LI $t2, 0
MV $l8, $t2
J func_ret_main

LI $t1, 0
BLE $l12, $t1, out_1
FSW $l20, 0(sp)
ADDI sp, sp, -4
FSW $l16, 0(sp)
ADDI sp, sp, -4
LI $t2, 1
SUB $t3, $l12, $t2
SW $t3, 0(sp)
ADDI sp, sp, -4
ADDI sp, sp, -4
SW ra, 0(sp)
ADDI sp, sp, -4
JR func_poly
ADDI sp, sp, 4
LW ra, 0(sp)
ADDI sp, sp, 4
FLW $f1, 0(sp)
ADDI sp, sp, 12
FMV.S $l20, $f1
out_1:
PUTS $gprompt
GETF $l-4
FMUL.S $f2, $l16, $l20
FADD.S $f3, $f2, $l-4
FMV.S $l8, $f3
J func_ret_poly

.section .text
;Current temp: null
;IR Code: 
MV fp, sp
JR func_main
HALT
;
func_main:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, -8
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
SW x7, 0(sp)
ADDI sp, sp, -4
FSW f0, 0(sp)
ADDI sp, sp, -4
FSW f1, 0(sp)
ADDI sp, sp, -4
FSW f2, 0(sp)
ADDI sp, sp, -4
FSW f3, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Ensurehack
FIMM.S f0, 0.0
;Ensurehack
FMV.S f1, f0
;Ensurehack
;Found free register x4
LI x4, 0
;Ensurehack
;Found free register x5
MV x5, x4
;Found free register x6
LA x3, 0x10000000
PUTS x3
;Ensurehack
GETF f2
;Found free register x7
LA x3, 0x10000004
PUTS x3
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
SW x4, -8(fp)
GETI x4
FSW f1, 0(sp)
ADDI sp, sp, -4
FSW f2, 0(sp)
ADDI sp, sp, -4
SW x4, 0(sp)
ADDI sp, sp, -4
ADDI sp, sp, -4
SW ra, 0(sp)
ADDI sp, sp, -4
JR func_poly
ADDI sp, sp, 4
LW ra, 0(sp)
ADDI sp, sp, 4
;Ensurehack
FLW f3, 0(sp)
ADDI sp, sp, 12
;Ensurehack
FMV.S f1, f3
PUTF f1
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
LA x3, 0x20000008
SW x4, 0(x3)
LI x4, 0
;Ensurehack
;Register is full, will allocate x5
;Freeing x5
SW x5, -4(fp)
MV x5, x4
;Storing Register at the end of BB block;
SW x5, 8(fp)
LA x3, 0x20000000
FSW f1, 0(x3)
LA x3, 0x20000004
FSW f2, 0(x3)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_main
func_ret_main:
ADDI sp, sp, 4
FLW f3, 0(sp)
ADDI sp, sp, 4
FLW f2, 0(sp)
ADDI sp, sp, 4
FLW f1, 0(sp)
ADDI sp, sp, 4
FLW f0, 0(sp)
ADDI sp, sp, 4
LW x7, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;
func_poly:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, -4
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
FSW f0, 0(sp)
ADDI sp, sp, -4
FSW f1, 0(sp)
ADDI sp, sp, -4
FSW f2, 0(sp)
ADDI sp, sp, -4
FSW f3, 0(sp)
ADDI sp, sp, -4
FSW f4, 0(sp)
ADDI sp, sp, -4
FSW f5, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Ensurehack
;Found free register x4
LI x4, 0
;Found free register x5
LW x5, 12(fp)
;Storing Register at the end of BB block;
BLE x5, x4, out_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
FLW f0, 20(fp)
FSW f0, 0(sp)
ADDI sp, sp, -4
FLW f1, 16(fp)
FSW f1, 0(sp)
ADDI sp, sp, -4
;Ensurehack
;Found free register x4
LI x4, 1
;Found free register x5
LW x5, 12(fp)
;Ensurehack
;Found free register x6
SUB x6, x5, x4
SW x6, 0(sp)
ADDI sp, sp, -4
ADDI sp, sp, -4
SW ra, 0(sp)
ADDI sp, sp, -4
JR func_poly
ADDI sp, sp, 4
LW ra, 0(sp)
ADDI sp, sp, 4
;Ensurehack
FLW f2, 0(sp)
ADDI sp, sp, 12
;Ensurehack
FMV.S f0, f2
;Storing Register at the end of BB block;
FSW f0, 20(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_1:
;Found free register x4
LA x3, 0x10000008
PUTS x3
;Ensurehack
GETF f0
FLW f1, 16(fp)
FLW f2, 20(fp)
;Ensurehack
FMUL.S f3, f1, f2
;Ensurehack
FADD.S f4, f3, f0
;Ensurehack
FMV.S f5, f4
;Storing Register at the end of BB block;
FSW f0, -4(fp)
FSW f5, 8(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_poly
func_ret_poly:
ADDI sp, sp, 4
FLW f5, 0(sp)
ADDI sp, sp, 4
FLW f4, 0(sp)
ADDI sp, sp, 4
FLW f3, 0(sp)
ADDI sp, sp, 4
FLW f2, 0(sp)
ADDI sp, sp, 4
FLW f1, 0(sp)
ADDI sp, sp, 4
FLW f0, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;


.section .strings
0x10000000 "Enter x value to evaluate: "
0x10000004 "Enter a degree: "
0x10000008 "Enter coefficient: "
