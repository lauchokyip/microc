package assembly.instructions;

public class Free extends Instruction {

    String src1;

    /**
     * Models the magic instruction MALLOC
     */
    public Free(String src) {
        super();
        this.src1 = src;
        this.oc = OpCode.FREE;
    }

    /**
     * @return "HALT"
     */
    public String toString() {
        return String.valueOf(this.oc) + " " + this.src1;
    }
}