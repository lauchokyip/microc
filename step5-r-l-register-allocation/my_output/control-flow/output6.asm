; Symbol table GLOBAL
; name a type INT location 0x20000000
; name b type INT location 0x20000004
; name s type STRING location 0x10000000 value "Enter a number "
; name found type STRING location 0x10000004 value "Found!\n"
; Function: INT main([])

; Symbol table main

PUTS $gs
GETI $ga
LI $t1, 2
MUL $t2, $t1, $ga
MV $gb, $t2
loop_1:
LI $t3, 0
BLE $gb, $t3, out_2
PUTI $gb
BNE $gb, $ga, out_1
PUTS $gfound
out_1:
LI $t4, 1
SUB $t5, $gb, $t4
MV $gb, $t5
J loop_1
out_2:
LI $t6, 0
MV $l8, $t6
J func_ret_main

.section .text
;Current temp: null
;IR Code: 
MV fp, sp
JR func_main
HALT
;
func_main:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, 0
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
SW x7, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x10000000
PUTS x3
;Ensurehack
;Found free register x5
GETI x5
;Ensurehack
;Found free register x6
LI x6, 2
;Ensurehack
;Found free register x7
MUL x7, x6, x5
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
MV x4, x7
;Storing Register at the end of BB block;
LA x3, 0x20000004
SW x4, 0(x3)
LA x3, 0x20000000
SW x5, 0(x3)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
loop_1:
;Ensurehack
;Found free register x4
LI x4, 0
;Found free register x5
LA x3, 0x20000004
LW x5, 0(x3)
;Storing Register at the end of BB block;
BLE x5, x4, out_2
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x20000004
LW x4, 0(x3)
PUTI x4
;Found free register x5
LA x3, 0x20000000
LW x5, 0(x3)
;Storing Register at the end of BB block;
BNE x4, x5, out_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x10000004
PUTS x3
;Storing Register at the end of BB block;
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_1:
;Ensurehack
;Found free register x4
LI x4, 1
;Found free register x5
LA x3, 0x20000004
LW x5, 0(x3)
;Ensurehack
;Found free register x6
SUB x6, x5, x4
;Ensurehack
MV x5, x6
;Storing Register at the end of BB block;
LA x3, 0x20000004
SW x5, 0(x3)
J loop_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_2:
;Ensurehack
;Found free register x4
LI x4, 0
;Ensurehack
;Found free register x5
MV x5, x4
;Storing Register at the end of BB block;
SW x5, 8(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_main
func_ret_main:
ADDI sp, sp, 4
LW x7, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;


.section .strings
0x10000000 "Enter a number "
0x10000004 "Found!\n"
