; Symbol table GLOBAL
; Function: INT foo([INT, INT])
; Function: INT bar([INT, INT])
; Function: INT main([])

; Symbol table main
; name a type INT location -4
; name b type INT location -8
; name c type INT location -12
; name d type INT location -16

; Symbol table foo
; name y type INT location 12
; name x type INT location 16

; Symbol table bar
; name y type INT location 12
; name x type INT location 16

GETI $l-4
GETI $l-8
SW $l-4, 0(sp)
ADDI sp, sp, -4
SW $l-8, 0(sp)
ADDI sp, sp, -4
ADDI sp, sp, -4
SW ra, 0(sp)
ADDI sp, sp, -4
JR func_foo
ADDI sp, sp, 4
LW ra, 0(sp)
ADDI sp, sp, 4
LW $t1, 0(sp)
ADDI sp, sp, 8
MV $l-12, $t1
SW $l-4, 0(sp)
ADDI sp, sp, -4
SW $l-8, 0(sp)
ADDI sp, sp, -4
ADDI sp, sp, -4
SW ra, 0(sp)
ADDI sp, sp, -4
JR func_bar
ADDI sp, sp, 4
LW ra, 0(sp)
ADDI sp, sp, 4
LW $t2, 0(sp)
ADDI sp, sp, 8
MV $l-16, $t2
PUTI $l-12
PUTI $l-16
LI $t3, 0
MV $l8, $t3
J func_ret_main

ADD $t1, $l16, $l12
MV $l8, $t1
J func_ret_foo

SUB $t1, $l16, $l12
MV $l8, $t1
J func_ret_bar

.section .text
;Current temp: null
;IR Code: 
MV fp, sp
JR func_main
HALT
;
func_main:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, -20
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
SW x7, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Ensurehack
;Found free register x4
GETI x4
;Ensurehack
;Found free register x5
GETI x5
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
ADDI sp, sp, -4
SW ra, 0(sp)
ADDI sp, sp, -4
JR func_foo
ADDI sp, sp, 4
LW ra, 0(sp)
ADDI sp, sp, 4
;Ensurehack
;Found free register x6
LW x6, 0(sp)
ADDI sp, sp, 8
;Ensurehack
;Found free register x7
MV x7, x6
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
ADDI sp, sp, -4
SW ra, 0(sp)
ADDI sp, sp, -4
JR func_bar
ADDI sp, sp, 4
LW ra, 0(sp)
ADDI sp, sp, 4
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
SW x4, -4(fp)
LW x4, 0(sp)
ADDI sp, sp, 8
;Ensurehack
;Register is full, will allocate x5
;Freeing x5
SW x5, -8(fp)
MV x5, x4
PUTI x7
PUTI x5
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
SW x4, -20(fp)
LI x4, 0
;Ensurehack
;Register is full, will allocate x5
;Freeing x5
SW x5, -16(fp)
MV x5, x4
;Storing Register at the end of BB block;
SW x5, 8(fp)
SW x7, -12(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_main
func_ret_main:
ADDI sp, sp, 4
LW x7, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;
func_foo:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, 0
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
SW x7, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LW x4, 16(fp)
;Found free register x5
LW x5, 12(fp)
;Ensurehack
;Found free register x6
ADD x6, x4, x5
;Ensurehack
;Found free register x7
MV x7, x6
;Storing Register at the end of BB block;
SW x7, 8(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_foo
func_ret_foo:
ADDI sp, sp, 4
LW x7, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;
func_bar:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, 0
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
SW x7, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LW x4, 16(fp)
;Found free register x5
LW x5, 12(fp)
;Ensurehack
;Found free register x6
SUB x6, x4, x5
;Ensurehack
;Found free register x7
MV x7, x6
;Storing Register at the end of BB block;
SW x7, 8(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_bar
func_ret_bar:
ADDI sp, sp, 4
LW x7, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;


.section .strings
