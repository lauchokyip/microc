; Symbol table GLOBAL
; name i type INT location 0x20000000
; name j type INT location 0x20000004
; name max type INT location 0x20000008
; name val type INT location 0x2000000c
; name is type STRING location 0x10000000 value "i: "
; name js type STRING location 0x10000004 value "j: "
; name newline type STRING location 0x10000008 value "\n"
; Function: INT main([])

; Symbol table main

LI $t1, 0
MV $gval, $t1
GETI $gmax
LI $t2, 0
MV $gi, $t2
loop_2:
BGE $gi, $gmax, out_2
PUTS $gis
PUTI $gi
PUTS $gnewline
MV $gj, $gi
loop_1:
BGE $gj, $gmax, out_1
PUTS $gjs
PUTI $gj
PUTS $gnewline
LI $t3, 1
ADD $t4, $gval, $t3
MV $gval, $t4
LI $t5, 1
ADD $t6, $gj, $t5
MV $gj, $t6
J loop_1
out_1:
LI $t7, 1
ADD $t8, $gi, $t7
MV $gi, $t8
J loop_2
out_2:
PUTI $gval
LI $t9, 0
MV $l8, $t9
J func_ret_main

.section .text
;Current temp: null
;IR Code: 
MV fp, sp
JR func_main
HALT
;
func_main:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, -12
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
SW x7, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Ensurehack
;Found free register x4
LI x4, 0
;Ensurehack
;Found free register x5
MV x5, x4
;Ensurehack
;Found free register x6
GETI x6
;Ensurehack
;Found free register x7
LI x7, 0
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
SW x4, -4(fp)
MV x4, x7
;Storing Register at the end of BB block;
LA x3, 0x20000000
SW x4, 0(x3)
LA x3, 0x2000000c
SW x5, 0(x3)
LA x3, 0x20000008
SW x6, 0(x3)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
loop_2:
;Found free register x4
LA x3, 0x20000000
LW x4, 0(x3)
;Found free register x5
LA x3, 0x20000008
LW x5, 0(x3)
;Storing Register at the end of BB block;
BGE x4, x5, out_2
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x10000000
PUTS x3
;Found free register x5
LA x3, 0x20000000
LW x5, 0(x3)
PUTI x5
;Found free register x6
LA x3, 0x10000008
PUTS x3
;Ensurehack
;Found free register x7
MV x7, x5
;Storing Register at the end of BB block;
LA x3, 0x20000004
SW x7, 0(x3)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
loop_1:
;Found free register x4
LA x3, 0x20000004
LW x4, 0(x3)
;Found free register x5
LA x3, 0x20000008
LW x5, 0(x3)
;Storing Register at the end of BB block;
BGE x4, x5, out_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x10000004
PUTS x3
;Found free register x5
LA x3, 0x20000004
LW x5, 0(x3)
PUTI x5
;Found free register x6
LA x3, 0x10000008
PUTS x3
;Ensurehack
;Found free register x7
LI x7, 1
;Register is full, will allocate x4
;Freeing x4
LA x3, 0x2000000c
LW x4, 0(x3)
;Ensurehack
;Register is full, will allocate x5
;Freeing x5
ADD x5, x4, x7
;Ensurehack
MV x4, x5
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
LA x3, 0x2000000c
SW x4, 0(x3)
LI x4, 1
;Register is full, will allocate x4
;Freeing x4
SW x4, -8(fp)
LA x3, 0x20000004
LW x4, 0(x3)
;Register is full, will allocate x5
;Freeing x5
SW x5, -12(fp)
LW x5, -8(fp)
;Ensurehack
;Register is full, will allocate x6
;Freeing x6
ADD x6, x4, x5
;Ensurehack
MV x4, x6
;Storing Register at the end of BB block;
LA x3, 0x20000004
SW x4, 0(x3)
J loop_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_1:
;Ensurehack
;Found free register x4
LI x4, 1
;Found free register x5
LA x3, 0x20000000
LW x5, 0(x3)
;Ensurehack
;Found free register x6
ADD x6, x5, x4
;Ensurehack
MV x5, x6
;Storing Register at the end of BB block;
LA x3, 0x20000000
SW x5, 0(x3)
J loop_2
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_2:
;Found free register x4
LA x3, 0x2000000c
LW x4, 0(x3)
PUTI x4
;Ensurehack
;Found free register x5
LI x5, 0
;Ensurehack
;Found free register x6
MV x6, x5
;Storing Register at the end of BB block;
SW x6, 8(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_main
func_ret_main:
ADDI sp, sp, 4
LW x7, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;


.section .strings
0x10000000 "i: "
0x10000004 "j: "
0x10000008 "\n"
