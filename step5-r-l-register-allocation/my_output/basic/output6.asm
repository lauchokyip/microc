; Symbol table GLOBAL
; name a type INT location 0x20000000
; name s type STRING location 0x10000000 value "Hello world\n"
; Function: INT main([])

; Symbol table main

.section .text
;Current temp: null
;IR Code: 
MV fp, sp
JR func_main
HALT
;
func_main:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, 0
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
SW x7, 0(sp)
ADDI sp, sp, -4
SW x9, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
LI x4, 10
MV x5, x4
PUTI x5
LA x3, 0x10000000
PUTS x3
LI x7, 0
MV x9, x7
;Storing Register at the end of BB block;
LA x3, 0x20000000
SW x5, 0(x3)
SW x9, 8(fp)
;End BB;
;//////////////////////////////////////////
J func_ret_main
func_ret_main:
ADDI sp, sp, 4
LW x9, 0(sp)
ADDI sp, sp, 4
LW x7, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;


.section .strings
0x10000000 "Hello world\n"
