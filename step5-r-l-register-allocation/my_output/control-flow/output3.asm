; Symbol table GLOBAL
; name a type INT location 0x20000000
; name b type INT location 0x20000004
; name c type INT location 0x20000008
; name d type INT location 0x2000000c
; name prompt type STRING location 0x10000000 value "enter a number "
; name output1 type STRING location 0x10000004 value "a less than b"
; name output2 type STRING location 0x10000008 value " and less than c \n"
; name output3 type STRING location 0x1000000c value " but not less than c \n"
; name output4 type STRING location 0x10000010 value "a greater than or equal to b"
; Function: INT main([])

; Symbol table main

PUTS $gprompt
GETI $ga
PUTS $gprompt
GETI $gb
PUTS $gprompt
GETI $gc
BGE $ga, $gb, else_3
PUTS $goutput1
BGE $ga, $gc, else_1
PUTS $goutput2
J out_1
else_1:
PUTS $goutput3
out_1:
J out_3
else_3:
PUTS $goutput4
BGE $ga, $gc, else_2
PUTS $goutput2
J out_2
else_2:
PUTS $goutput3
out_2:
out_3:
LI $t1, 0
MV $l8, $t1
J func_ret_main

.section .text
;Current temp: null
;IR Code: 
MV fp, sp
JR func_main
HALT
;
func_main:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, 0
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
SW x7, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x10000000
PUTS x3
;Ensurehack
;Found free register x5
GETI x5
PUTS x3
;Ensurehack
;Found free register x6
GETI x6
PUTS x3
;Ensurehack
;Found free register x7
GETI x7
;Storing Register at the end of BB block;
LA x3, 0x20000000
SW x5, 0(x3)
LA x3, 0x20000004
SW x6, 0(x3)
LA x3, 0x20000008
SW x7, 0(x3)
BGE x5, x6, else_3
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x10000004
PUTS x3
;Found free register x5
LA x3, 0x20000000
LW x5, 0(x3)
;Found free register x6
LA x3, 0x20000008
LW x6, 0(x3)
;Storing Register at the end of BB block;
BGE x5, x6, else_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x10000008
PUTS x3
;Storing Register at the end of BB block;
J out_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
else_1:
;Found free register x4
LA x3, 0x1000000c
PUTS x3
;Storing Register at the end of BB block;
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_1:
;Storing Register at the end of BB block;
J out_3
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
else_3:
;Found free register x4
LA x3, 0x10000010
PUTS x3
;Found free register x5
LA x3, 0x20000000
LW x5, 0(x3)
;Found free register x6
LA x3, 0x20000008
LW x6, 0(x3)
;Storing Register at the end of BB block;
BGE x5, x6, else_2
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x10000008
PUTS x3
;Storing Register at the end of BB block;
J out_2
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
else_2:
;Found free register x4
LA x3, 0x1000000c
PUTS x3
;Storing Register at the end of BB block;
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_2:
;Storing Register at the end of BB block;
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_3:
;Ensurehack
;Found free register x4
LI x4, 0
;Ensurehack
;Found free register x5
MV x5, x4
;Storing Register at the end of BB block;
SW x5, 8(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_main
func_ret_main:
ADDI sp, sp, 4
LW x7, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;


.section .strings
0x10000000 "enter a number "
0x10000004 "a less than b"
0x10000008 " and less than c \n"
0x1000000c " but not less than c \n"
0x10000010 "a greater than or equal to b"
