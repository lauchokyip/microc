; Symbol table GLOBAL
; name cur type INT location 0x20000000
; Function: INT main([])

; Symbol table main

LI $t1, 0
MV $gcur, $t1
loop_1:
LI $t2, 10
BGE $gcur, $t2, out_1
LI $t3, 4
ADD $t4, $gcur, $t3
MV $gcur, $t4
J loop_1
out_1:
PUTI $gcur
LI $t5, 0
MV $l8, $t5
J func_ret_main

.section .text
;Current temp: null
;IR Code: 
MV fp, sp
JR func_main
HALT
;
func_main:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, 0
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Ensurehack
;Found free register x4
LI x4, 0
;Ensurehack
;Found free register x5
MV x5, x4
;Storing Register at the end of BB block;
LA x3, 0x20000000
SW x5, 0(x3)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
loop_1:
;Ensurehack
;Found free register x4
LI x4, 10
;Found free register x5
LA x3, 0x20000000
LW x5, 0(x3)
;Storing Register at the end of BB block;
BGE x5, x4, out_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Ensurehack
;Found free register x4
LI x4, 4
;Found free register x5
LA x3, 0x20000000
LW x5, 0(x3)
;Ensurehack
;Found free register x6
ADD x6, x5, x4
;Ensurehack
MV x5, x6
;Storing Register at the end of BB block;
LA x3, 0x20000000
SW x5, 0(x3)
J loop_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
out_1:
;Found free register x4
LA x3, 0x20000000
LW x4, 0(x3)
PUTI x4
;Ensurehack
;Found free register x5
LI x5, 0
;Ensurehack
;Found free register x6
MV x6, x5
;Storing Register at the end of BB block;
SW x6, 8(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_main
func_ret_main:
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;


.section .strings
