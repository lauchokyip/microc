; Symbol table GLOBAL
; name factstr type STRING location 0x10000000 value "Enter a number: "
; name outpref type STRING location 0x10000004 value "Factorial of "
; name outsuff type STRING location 0x10000008 value " is "
; name x type INT location 0x20000000
; Function: INT fact([INT])
; Function: INT main([])

; Symbol table main
; name res type INT location -4

; Symbol table fact
; name n type INT location 12

PUTS $gfactstr
GETI $gx
LI $t1, 2
ADD $t2, $gx, $t1
SW $t2, 0(sp)
ADDI sp, sp, -4
ADDI sp, sp, -4
SW ra, 0(sp)
ADDI sp, sp, -4
JR func_fact
ADDI sp, sp, 4
LW ra, 0(sp)
ADDI sp, sp, 4
LW $t3, 0(sp)
ADDI sp, sp, 4
MV $l-4, $t3
PUTS $goutpref
LI $t4, 2
ADD $t5, $gx, $t4
PUTI $t5
PUTS $goutsuff
PUTI $l-4
LI $t6, 0
MV $l8, $t6
J func_ret_main

LI $t1, 1
BGT $l12, $t1, else_1
LI $t2, 1
MV $l8, $t2
J func_ret_fact
J out_1
else_1:
LI $t3, 1
SUB $t4, $l12, $t3
SW $t4, 0(sp)
ADDI sp, sp, -4
ADDI sp, sp, -4
SW ra, 0(sp)
ADDI sp, sp, -4
JR func_fact
ADDI sp, sp, 4
LW ra, 0(sp)
ADDI sp, sp, 4
LW $t5, 0(sp)
ADDI sp, sp, 4
MUL $t6, $l12, $t5
MV $l8, $t6
J func_ret_fact
out_1:

.section .text
;Current temp: null
;IR Code: 
MV fp, sp
JR func_main
HALT
;
func_main:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, -16
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
SW x7, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Found free register x4
LA x3, 0x10000000
PUTS x3
;Ensurehack
;Found free register x5
GETI x5
;Ensurehack
;Found free register x6
LI x6, 2
;Ensurehack
;Found free register x7
ADD x7, x5, x6
SW x7, 0(sp)
ADDI sp, sp, -4
ADDI sp, sp, -4
SW ra, 0(sp)
ADDI sp, sp, -4
JR func_fact
ADDI sp, sp, 4
LW ra, 0(sp)
ADDI sp, sp, 4
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
LW x4, 0(sp)
ADDI sp, sp, 4
;Ensurehack
;Register is full, will allocate x5
;Freeing x5
LA x3, 0x20000000
SW x5, 0(x3)
MV x5, x4
;Register is full, will allocate x4
;Freeing x4
SW x4, -8(fp)
LA x3, 0x10000004
PUTS x3
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
LI x4, 2
;Register is full, will allocate x4
;Freeing x4
SW x4, -12(fp)
LA x3, 0x20000000
LW x4, 0(x3)
;Register is full, will allocate x5
;Freeing x5
SW x5, -4(fp)
LW x5, -12(fp)
;Ensurehack
;Register is full, will allocate x6
;Freeing x6
SW x6, -16(fp)
ADD x6, x4, x5
PUTI x6
;Register is full, will allocate x4
;Freeing x4
LA x3, 0x10000008
PUTS x3
;Register is full, will allocate x4
;Freeing x4
LW x4, -4(fp)
PUTI x4
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
LI x4, 0
;Ensurehack
;Register is full, will allocate x5
;Freeing x5
MV x5, x4
;Storing Register at the end of BB block;
SW x5, 8(fp)
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
J func_ret_main
func_ret_main:
ADDI sp, sp, 4
LW x7, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;
func_fact:
SW fp, 0(sp)
MV fp, sp
ADDI sp, sp, -4
ADDI sp, sp, -4
SW x4, 0(sp)
ADDI sp, sp, -4
SW x5, 0(sp)
ADDI sp, sp, -4
SW x6, 0(sp)
ADDI sp, sp, -4
SW x7, 0(sp)
ADDI sp, sp, -4
;//////////////////////////////////////////
;Starting BB;
;Ensurehack
;Found free register x4
LI x4, 1
;Found free register x5
LW x5, 12(fp)
;Storing Register at the end of BB block;
BGT x5, x4, else_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Ensurehack
;Found free register x4
LI x4, 1
;Ensurehack
;Found free register x5
MV x5, x4
;Storing Register at the end of BB block;
SW x5, 8(fp)
J func_ret_fact
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
;Storing Register at the end of BB block;
J out_1
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
;//////////////////////////////////////////
;Starting BB;
else_1:
;Ensurehack
;Found free register x4
LI x4, 1
;Found free register x5
LW x5, 12(fp)
;Ensurehack
;Found free register x6
SUB x6, x5, x4
SW x6, 0(sp)
ADDI sp, sp, -4
ADDI sp, sp, -4
SW ra, 0(sp)
ADDI sp, sp, -4
JR func_fact
ADDI sp, sp, 4
LW ra, 0(sp)
ADDI sp, sp, 4
;Ensurehack
;Found free register x7
LW x7, 0(sp)
ADDI sp, sp, 4
;Ensurehack
;Register is full, will allocate x4
;Freeing x4
SW x4, -4(fp)
MUL x4, x5, x7
;Ensurehack
;Register is full, will allocate x5
;Freeing x5
MV x5, x4
;Storing Register at the end of BB block;
SW x5, 8(fp)
J func_ret_fact
;Freeing x7
;Freeing x6
;Freeing x5
;Freeing x4
;Freeing f8
;Freeing f7
;Freeing f6
;Freeing f5
;Freeing f4
;Freeing f3
;Freeing f2
;Freeing f1
;Freeing f0
;End BB;
;//////////////////////////////////////////
out_1:
func_ret_fact:
ADDI sp, sp, 4
LW x7, 0(sp)
ADDI sp, sp, 4
LW x6, 0(sp)
ADDI sp, sp, 4
LW x5, 0(sp)
ADDI sp, sp, 4
LW x4, 0(sp)
MV sp, fp
LW fp, 0(fp)
RET
;


.section .strings
0x10000000 "Enter a number: "
0x10000004 "Factorial of "
0x10000008 " is "
